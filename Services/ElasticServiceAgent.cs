﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule.Services
{
    public class ElasticServiceAgent : NotifyPropertyChange
    {
        private object agentLocker = new object();
        private Globals globals;

        private ResultGetControllerViews resultGetControllerViews;
        public ResultGetControllerViews ResultGetControllerViews
        {
            get
            {
                lock(agentLocker)
                {
                    return resultGetControllerViews;
                }
            }
            set
            {
                lock(agentLocker)
                {
                    resultGetControllerViews = value;
                }
                RaisePropertyChanged(nameof(ResultGetControllerViews));
            }
        }

        private clsOntologyItem oItem;
        public clsOntologyItem OItem
        {
            get
            {
                lock (agentLocker)
                {
                    return oItem;
                }
            }
            private set
            {
                lock (agentLocker)
                {
                    oItem = value;
                }
                RaisePropertyChanged(nameof(OItem));
            }
        }

        private ResultGetViewToClassOrderId resultGetViewToClassOrderId;
        public ResultGetViewToClassOrderId ResultGetViewToClassOrderId
        {
            get
            {
                lock (agentLocker)
                {
                    return resultGetViewToClassOrderId;
                }
            }
            private set
            {
                lock (agentLocker)
                {
                    resultGetViewToClassOrderId = value;
                }
                RaisePropertyChanged(nameof(ResultGetViewToClassOrderId));
            }
        }

        public async Task<ResultGetViewToClassOrderId> GetViewToClassOrderId(string idView, string idObject)
        {
            var taskResult = await Task.Run<ResultGetViewToClassOrderId>(() =>
            {
                
                var searchOrderId = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idView,
                        ID_RelationType = globals.RelationType_belongingClass.GUID,
                        ID_Other = idObject
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);
                var result = new ResultGetViewToClassOrderId
                {
                    Result = dbReader.GetDataObjectRel(searchOrderId, doIds: true)
                };

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultGetViewToClassOrderId = result;
                    return result;
                }

                result.OrderId = 0;
                if (dbReader.ObjectRelsId.Any())
                {
                    result.OrderId = dbReader.ObjectRelsId.First().OrderIDNotNull;
                }

                ResultGetViewToClassOrderId = result;
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> GetOItem(string id, string type)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                OItem = dbReader.GetOItem(id, type);
                return oItem;
            });

            return taskResult;
        }

        public async Task<ResultGetControllerViews> GetControllerViews(List<ViewMetaItem> viewItems)
        {
            var taskResult = await Task.Run<ResultGetControllerViews>(() =>
            {
                var result = new ResultGetControllerViews
                {
                    Result = globals.LState_Success.Clone()
                };

                
                var searchViewToClass = viewItems.Select(viewRel => new clsObjectRel
                {
                    ID_Object = viewRel.IdView,
                    ID_RelationType = globals.RelationType_belongingClass.GUID
                }).ToList();

                if (searchViewToClass.Any())
                {
                    var dbReaderViewToClass = new OntologyModDBConnector(globals);
                    result.Result = dbReaderViewToClass.GetDataObjectRel(searchViewToClass);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultGetControllerViews = result;
                        return result;
                    }

                    result.ViewToClass = dbReaderViewToClass.ObjectRels;
                }
                else
                {
                    result.ViewToClass = new List<clsObjectRel>();
                }


                ResultGetControllerViews = result;
                return result;
            });
            return taskResult;
        }

        public ElasticServiceAgent(Globals globals)
        {
            this.globals = globals;
        }
    }

    

    public class ResultGetControllerViews
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> Controller { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ControllerModuleRel { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ModuleFunctionRel { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ControllerViewRel { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ViewToClass { get; set; } = new List<clsObjectRel>();
    }

    public class ResultGetViewToClassOrderId
    {
        public clsOntologyItem Result { get; set; }
        public long OrderId { get; set; }
    }
}
