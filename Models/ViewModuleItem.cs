﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule.Models
{

    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]



    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoSortable(mode = SortType.sortable)]
    public class ViewModuleItem
    {
        private string idView;
        [KendoColumn(hidden = true)]
        public string IdView
        {
            get { return idView; }
            set
            {
                if (idView == value) return;

                idView = value;

            }
        }

        private string nameView;
        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "View")]
        public string NameView
        {
            get { return nameView; }
            set
            {
                if (nameView == value) return;

                nameView = value;

            }
        }

        private string idModule;

        [KendoColumn(hidden = true)]
        public string IdModule
        {
            get { return idModule; }
            set
            {
                if (idModule == value) return;

                idModule = value;


            }
        }

        private string nameModule;

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Module")]
        public string NameModule
        {
            get { return nameModule; }
            set
            {
                if (nameModule == value) return;

                nameModule = value;

            }
        }

        private string idModuleFunction;
        [KendoColumn(hidden = true)]
        public string IdModuleFunction
        {
            get { return idModuleFunction; }
            set
            {
                if (idModuleFunction == value) return;

                idModuleFunction = value;

            }
        }

        private string nameModuleFunction;
        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Function")]
        public string NameModuleFunction
        {
            get { return nameModuleFunction; }
            set
            {
                if (nameModuleFunction == value) return;

                nameModuleFunction = value;

            }
        }

        private string idController;
        [KendoColumn(hidden = true)]
        public string IdController
        {
            get { return idController; }
            set
            {
                if (idController == value) return;

                idController = value;

            }
        }

        private string nameController;
        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Function")]
        public string NameController
        {
            get { return nameController; }
            set
            {
                if (nameController == value) return;

                nameController = value;

            }
        }

        private string idObject;
        [KendoColumn(hidden = true)]
        public string IdObject
        {
            get { return idObject; }
            set
            {
                if (idObject == value) return;

                idObject = value;

            }
        }

        private string nameObject;
        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Link", template = "<button class='modules-link-button'>Open</button>")]
        public string NameObject
        {
            get { return nameObject; }
            set
            {
                if (nameObject == value) return;

                nameObject = value;

            }
        }

        private long count;
        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Count")]
        [KendoColumnSortable(initialDirection = InitialDirection.desc, sortable = true)]
        public long Count
        {
            get { return count; }
            set
            {
                if (count == value) return;
                count = value;
            }
        }
    }
}
