﻿using ModuleManagementModule.Models;
using ModuleManagementModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule.Factories
{
    public class ViewModelFactory : NotifyPropertyChange
    {
        private object factoryLocker = new object();
        private Globals globals;

        private ResultCreateViewModuleItems resultCreateViewModuleItems;
        public ResultCreateViewModuleItems ResultCreateViewModuleItems
        {
            get
            {
                lock(factoryLocker)
                {
                    return resultCreateViewModuleItems;
                }
                
            }
            set
            {
                lock(factoryLocker)
                {
                    resultCreateViewModuleItems = value;
                }
                RaisePropertyChanged(nameof(ResultCreateViewModuleItems));
            }
        }

        private ResultCreateViewItemsForNavigation resultCreateViewItemsForNavigation;
        public ResultCreateViewItemsForNavigation ResultCreateViewItemsForNavigation
        {
            get
            {
                lock (factoryLocker)
                {
                    return resultCreateViewItemsForNavigation;
                }

            }
            set
            {
                lock (factoryLocker)
                {
                    resultCreateViewItemsForNavigation = value;
                }
                RaisePropertyChanged(nameof(ResultCreateViewItemsForNavigation));
            }
        }

        public async Task<ResultCreateViewModuleItems> CreateViewModuleItems(ResultGetControllerViews controllerViewLists, clsOntologyItem oItemObject, List<ViewMetaItem> viewItems)
        {
            var taskResult = await Task.Run<ResultCreateViewModuleItems>(() =>
            {
                var result = new ResultCreateViewModuleItems
                {
                    Result = globals.LState_Success.Clone()
                };

                result.ViewModelItems = (from view in viewItems
                                         join moduleFunction in viewItems.SelectMany(viewItm => viewItm.ModuleFunctions) on view.IdModule equals moduleFunction.GUID_Related into moduleFunctions
                                         from moduleFunction in moduleFunctions.DefaultIfEmpty()
                                         select new 
                                         {
                                             IdController = view.IdController,
                                             NameController = view.NameController,
                                             IdView = view.IdView,
                                             NameView = view.NameView,
                                             IdModule = view.IdModule,
                                             NameModule = view.NameModule,
                                             IdModuleFunction = moduleFunction != null ? moduleFunction.GUID : null,
                                             NameModuleFunction = moduleFunction != null ? moduleFunction.Name : null,
                                             IdObject = oItemObject != null ? oItemObject.GUID : null,
                                             NameObject = oItemObject != null ? oItemObject.Name : null
                                         }).GroupBy(viewToModule => new 
                                         {
                                             IdController = viewToModule.IdController,
                                             NameController = viewToModule.NameController,
                                             IdView = viewToModule.IdView,
                                             NameView = viewToModule.NameView,
                                             IdModule = viewToModule.IdModule,
                                             NameModule = viewToModule.NameModule,
                                             IdModuleFunction = viewToModule.IdModuleFunction,
                                             NameModuleFunction = viewToModule.NameModuleFunction,
                                             IdObject = viewToModule.IdObject,
                                             NameObject = viewToModule.NameObject
                                         }).Select(group => new ViewModuleItem
                                         {
                                             IdController = group.Key.IdController,
                                             NameController = group.Key.NameController,
                                             IdView = group.Key.IdView,
                                             NameView = group.Key.NameView,
                                             IdModule = group.Key.IdModule,
                                             NameModule = group.Key.NameModule,
                                             IdModuleFunction = group.Key.IdModuleFunction,
                                             NameModuleFunction = group.Key.NameModuleFunction,
                                             IdObject = group.Key.IdObject,
                                             NameObject = group.Key.NameObject
                                         }).ToList();

                foreach (var viewModule in result.ViewModelItems)
                {
                    var classRel = controllerViewLists.ViewToClass.Where(viewToClass => viewToClass.ID_Object == viewModule.IdView);
                    if (oItemObject == null)
                    {
                        viewModule.Count = classRel.Sum(clsRl => clsRl.OrderID.Value);
                    }
                    else
                    {
                        var viewClasses = classRel.Where(clsRl => clsRl.ID_Other == oItemObject.GUID_Parent);
                        viewModule.Count = viewClasses.Sum(viewCls => viewCls.OrderID.Value);
                    }
                }

                ResultCreateViewModuleItems = result;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultCreateViewItemsForNavigation> CreateViewItemsForNavigation(List<ViewMetaItem> viewItems)
        {
            var taskResult = await Task.Run<ResultCreateViewItemsForNavigation>(() =>
            {
                var result = new ResultCreateViewItemsForNavigation
                {
                    Result = globals.LState_Success.Clone(),
                    ViewItems = new List<Dictionary<string, object>>()
                };

                var dictRoot = new Dictionary<string, object>();
                dictRoot.Add(nameof(KendoTreeListItem.Id), globals.Root.GUID);
                dictRoot.Add(nameof(KendoTreeListItem.Name), globals.Root.Name);
                dictRoot.Add(nameof(KendoTreeListItem.IdParent), null);
                
                result.ViewItems.Add(dictRoot);

                //var moduleFunctionsToDict = viewItems.ModuleFunctions.GroupBy(moduleFunction => new { IdFunction = moduleFunction.ID_Other, NameFunction = moduleFunction.Name_Other });

                //foreach (var moduleFunction in moduleFunctionsToDict)
                //{
                //    var dictFunction = new Dictionary<string, object>();
                //    dictFunction.Add(nameof(KendoTreeListItem.Id), moduleFunction.Key.IdFunction);
                //    dictFunction.Add(nameof(KendoTreeListItem.Name), moduleFunction.Key.NameFunction);
                //    dictFunction.Add(nameof(KendoTreeListItem.IdParent), globals.Root.GUID);
                //    result.ViewItems.Add(dictFunction);
                //}


                //var viewItems = (from controller in controllerViewLists.Controller
                //                 join view in controllerViewLists.ControllerViewRel on controller.GUID equals view.ID_Object
                //                 join moduleFunction in moduleFunctions on controller.GUID equals moduleFunction.ControllerModuleRel.ID_Object
                //                 select new { Controller = controller, View = view, ModuleFunction = moduleFunction });


                foreach (var viewItem in viewItems)
                {
                    var dictView = new Dictionary<string, object>();
                    dictView.Add(nameof(KendoTreeListItem.Id), viewItem.IdView);
                    dictView.Add(nameof(KendoTreeListItem.Name), viewItem.NameView);
                    dictView.Add(nameof(KendoTreeListItem.IdParent), null);
                    result.ViewItems.Add(dictView);
                }


                ResultCreateViewItemsForNavigation = result;
                return result;
            });

            return taskResult;
        }

        public ViewModelFactory(Globals globals)
        {
            this.globals = globals;
        }
    }


    public class ResultCreateViewModuleItems
    {
        public clsOntologyItem Result { get; set; }
        public List<ViewModuleItem> ViewModelItems { get; set; }
    }

    public class ResultCreateViewItemsForNavigation
    {
        public clsOntologyItem Result { get; set; }
        public List<Dictionary<string, object>> ViewItems { get; set; }
        public ResultGetControllerViews ControllerViews { get; set; }

    }
}
