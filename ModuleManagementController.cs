﻿using ModuleManagementModule.Factories;
using ModuleManagementModule.Models;
using ModuleManagementModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule
{
    public class ModuleManagementController : AppController
    {
        private object controllerLocker = new object();

        private ElasticServiceAgent serviceAgentElastic;
        private ViewModelFactory factory;
        private clsTransaction transaction;
        private clsRelationConfig relationConfig;
        private TreeFactory treeFactory;

        private ResultCreateViewModuleItems resultCreateViewModuleItems;
        public ResultCreateViewModuleItems ResultCreateViewModuleItems
        {
            get
            {
                lock(controllerLocker)
                {
                    return resultCreateViewModuleItems;
                }
                
            }
            set
            {
                lock (controllerLocker)
                {
                    resultCreateViewModuleItems = value;
                }
            }
        }

        private clsOntologyItem resultSetViewToClass;
        public clsOntologyItem ResultSetViewToClass
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultSetViewToClass;
                }

            }
            set
            {
                lock (controllerLocker)
                {
                    resultSetViewToClass = value;
                }
            }
        }

        private ResultRefItem resultRefItem;
        public ResultRefItem ResultRefItem
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultRefItem;
                }

            }
            set
            {
                lock (controllerLocker)
                {
                    resultRefItem = value;
                }
            }
        }

        
        private FactoryResultTreeListConfig resultTreeListConfig;
        public FactoryResultTreeListConfig ResultTreeListConfig
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultTreeListConfig;
                }

            }
            set
            {
                lock (controllerLocker)
                {
                    resultTreeListConfig = value;
                }
            }
        }

        private ResultCreateViewItemsForNavigation resultCreateViewItemsForNavigation;
        public ResultCreateViewItemsForNavigation ResultCreateViewItemsForNavigation
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultCreateViewItemsForNavigation;
                }

            }
            set
            {
                lock (controllerLocker)
                {
                    resultCreateViewItemsForNavigation = value;
                }
            }
        }

        private ResultGetActionView resultGetActionView;
        public ResultGetActionView ResultGetActionView
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultGetActionView;
                }

            }
            set
            {
                lock (controllerLocker)
                {
                    resultGetActionView = value;
                }
            }
        }

        public async Task<ResultItem<clsOntologyItem>> GetRefItem(string idObject)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(async() =>
            {
                var oitemTaskObject = await serviceAgentElastic.GetOItem(idObject, Globals.Type_Object);

                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = oitemTaskObject
                };
                return result;
            });

            return taskResult;
        }

        public async Task<ResultRefItem> GetRefItemTitle(string idObject)
        {
            var taskResult = await Task.Run<ResultRefItem>(() =>
            {
                var oitemTaskObject = serviceAgentElastic.GetOItem(idObject, Globals.Type_Object);
                oitemTaskObject.Wait();

                var oitemTaskClass = serviceAgentElastic.GetOItem(oitemTaskObject.Result.GUID_Parent, Globals.Type_Class);
                oitemTaskClass.Wait();

                var resultRefItem = new ResultRefItem
                {
                    Result = Globals.LState_Success.Clone(),
                    Title = $"{oitemTaskClass.Result.Name}\\{oitemTaskObject.Result.Name}"
                };

                ResultRefItem = resultRefItem;
                return resultRefItem;
            });

            return taskResult;
        }

        public async Task<ResultCreateViewModuleItems> GetViewModuleItems(string idObject, List<ViewMetaItem> viewItems)
        {
            var taskResult = await Task.Run<ResultCreateViewModuleItems>(async() =>
            {
                var result = new ResultCreateViewModuleItems
                {
                    Result = Globals.LState_Success.Clone()
                };

                clsOntologyItem oItemObject = null;
                if (!string.IsNullOrEmpty(idObject))
                {
                    var resultOItemTask = await serviceAgentElastic.GetOItem(idObject, Globals.Type_Object);
                    oItemObject = resultOItemTask;
                }


                var resultTaskAgent = await serviceAgentElastic.GetControllerViews(viewItems);

                var resultTaskFactory = await factory.CreateViewModuleItems(resultTaskAgent, oItemObject, viewItems);

                ResultCreateViewModuleItems = resultTaskFactory;
                return resultTaskFactory;
            });


            return taskResult;
        }

        public async Task<ResultCreateViewItemsForNavigation> GetViewItems(List<ViewMetaItem> viewItems)
        {
            var result = new ResultCreateViewModuleItems
            {
                Result = Globals.LState_Success.Clone()
            };


            //var resultTaskAgent = await serviceAgentElastic.GetControllerViews(viewItems);

            var resultTaskFactory = await factory.CreateViewItemsForNavigation(viewItems);

            ResultCreateViewItemsForNavigation = resultTaskFactory;
            return resultTaskFactory;
        }

        public async Task<ResultGetActionView> GetActionView(CreateViewListResult controllerViews, string idView)
        {
            var result = new ResultGetActionView
            {
                Result = Globals.LState_Success.Clone(),

            };

            var view = controllerViews.ViewList.FirstOrDefault(contrlView => contrlView.IdView == idView);
            if (view != null)
            {
                result.Action = view.NameController;
                result.View = view.NameView;
            }

            ResultGetActionView = result;
            return result;
        }

        public async Task<FactoryResultTreeListConfig> GetViewTreeListConfig(string dataUrl)
        {
            var resultTask = await treeFactory.CreateKendoTreeListConfig(typeof(KendoTreeListItem), dataUrl);
            ResultTreeListConfig = resultTask;
            return resultTask;
        }

        public async Task<clsOntologyItem> SetViewToClass(string idObject, string idView)
        {
            clsOntologyItem classItem;
            if (!string.IsNullOrEmpty(idObject))
            {
                var oitemTaskObject = await serviceAgentElastic.GetOItem(idObject, Globals.Type_Object);
                var oitemTaskClass = await serviceAgentElastic.GetOItem(oitemTaskObject.GUID_Parent, Globals.Type_Class);
                classItem = oitemTaskClass;
            }
            else
            {
                classItem = Config.LocalData.Class_Module;
            }
            

            var oitemTaskView = await serviceAgentElastic.GetOItem(idView, Globals.Type_Object);

            var orderIdTask = await serviceAgentElastic.GetViewToClassOrderId(idView, classItem.GUID);

            var relViewToClass = relationConfig.Rel_ObjectRelation(oitemTaskView, classItem, Globals.RelationType_belongingClass, orderId: orderIdTask.OrderId+1);
            transaction.ClearItems();
            var result = transaction.do_Transaction(relViewToClass);

            ResultSetViewToClass = result;
            return result;

        }


        public ModuleManagementController(Globals Globals) : base(Globals)
        {
            Initialize();
        }

        private void Initialize()
        {
            serviceAgentElastic = new ElasticServiceAgent(Globals);
            factory = new ViewModelFactory(Globals);
            transaction = new clsTransaction(Globals);
            relationConfig = new clsRelationConfig(Globals);
            treeFactory = new TreeFactory(Globals);
        }
    }

    public class ResultRefItem
    {
        public clsOntologyItem Result { get; set; }
        public string Title { get; set; }
    }

    public class ResultGetActionView
    {
        public clsOntologyItem Result { get; set; }
        public string Action { get; set; }
        public string View { get; set; }
    }
    
}
